.PHONY: dev_image dev_container build publish

dev_image:
	docker build -t joblibgcs -f dev.dockerfile .

dev_container:
	docker run -it --rm -d --name joblibgcs --env-file .env.container -v $(shell pwd):/joblibgcs joblibgcs	

build:
	poetry build

publish:
	poetry publish